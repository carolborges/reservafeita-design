(function($) {
    $(function() {
        var jcarousel = $('.slidehead');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function() {
                var carousel = $(this),
                    width = carousel.innerWidth();

                // if (width >= 600) {
                //   width = width / 3;
                // } else if (width >= 350) {
                //   width = width / 2;
                // }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px').jcarouselSwipe();
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.slidehead-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.slidehead-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.slidehead-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page, carouselItems) {
                    return '<a class="d-flex justify-content-center align-items-center" href="#' + page + '">' + $(carouselItems).children().attr("alt") + '</a>';
                }
            });
    });
})(jQuery);